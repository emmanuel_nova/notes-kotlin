package me.manenova.notes.ui.notes

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import me.manenova.notes.R
import me.manenova.notes.core.Resource
import me.manenova.notes.data.remote.ApiClient
import me.manenova.notes.data.remote.NoteDataSource
import me.manenova.notes.databinding.FragmentNotesBinding
import me.manenova.notes.presentation.NoteViewModel
import me.manenova.notes.presentation.NoteViewModelFactory
import me.manenova.notes.repository.NoteRepositoryImp
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import me.manenova.notes.data.local.AppDatabase
import me.manenova.notes.data.local.LocalDataSource
import me.manenova.notes.data.model.Note
import me.manenova.notes.ui.notes.adapters.NoteAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout




class NotesFragment : Fragment(R.layout.fragment_notes) {

    private lateinit var binding: FragmentNotesBinding
    private lateinit var adapter: NoteAdapter

    private val viewModel by viewModels<NoteViewModel> {
        NoteViewModelFactory(NoteRepositoryImp(
            LocalDataSource(AppDatabase.getDataBase(this.requireContext()).noteDao()),
            NoteDataSource(ApiClient.service)))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentNotesBinding.bind(view)

        binding.recyclerNotes.layoutManager = GridLayoutManager (requireContext(),2)

        binding.swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light);

        binding.btnAddNote.setOnClickListener {
            val action = NotesFragmentDirections.actionNotesFragmentToNoteEditFragment()
            findNavController().navigate(action)
        }

        binding.swipeContainer.setOnRefreshListener {

            binding.recyclerNotes.adapter = null

            viewModel.fetchNotes().observe(viewLifecycleOwner, Observer { result ->
                when(result){
                    is Resource.Loading -> {
                        //binding.progressbar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        //binding.progressbar.visibility = View.GONE

                        adapter = NoteAdapter(result.data.data){ note ->
                            onNoteClick(note)

                        }

                        binding.recyclerNotes.adapter = adapter

                        Log.d("LiveData","${result.data.toString()}")
                        binding.swipeContainer.setRefreshing(false)
                    }
                    is Resource.Failure -> {
                        binding.swipeContainer.setRefreshing(false)
                        Log.d("LiveData","${result.exception.toString()}")
                    }
                }
            })


        }

        viewModel.fetchNotes().observe(viewLifecycleOwner, Observer { result ->
            when(result){
                is Resource.Loading -> {
                    binding.progressbar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.progressbar.visibility = View.GONE

                    adapter = NoteAdapter(result.data.data){ note ->
                       onNoteClick(note)

                    }

                    binding.recyclerNotes.adapter = adapter

                    Log.d("LiveData","${result.data.toString()}")
                }
                is Resource.Failure -> {
                    binding.progressbar.visibility = View.GONE
                    Log.d("LiveData","${result.exception.toString()}")
                }
            }
        })
    }

    private fun onNoteClick(note: Note){
        val action = NotesFragmentDirections.actionNotesFragmentToNoteDetailFragment(
            note.id,
            note.title,
            note.content,
            note.image
        )

        findNavController().navigate(action)
    }
}