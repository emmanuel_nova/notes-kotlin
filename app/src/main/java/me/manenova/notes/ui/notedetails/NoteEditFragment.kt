package me.manenova.notes.ui.notedetails


import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer

import androidx.navigation.fragment.findNavController
import me.manenova.notes.R
import me.manenova.notes.core.Resource
import me.manenova.notes.data.local.AppDatabase
import me.manenova.notes.data.local.LocalDataSource
import me.manenova.notes.data.model.Note
import me.manenova.notes.data.remote.ApiClient
import me.manenova.notes.data.remote.NoteDataSource
import me.manenova.notes.databinding.FragmentNoteEditBinding
import me.manenova.notes.presentation.NoteViewModel
import me.manenova.notes.presentation.NoteViewModelFactory
import me.manenova.notes.repository.NoteRepositoryImp


class NoteEditFragment : Fragment(R.layout.fragment_note_edit) {

    private lateinit var binding: FragmentNoteEditBinding

    private val viewModel by viewModels<NoteViewModel> {
        NoteViewModelFactory(NoteRepositoryImp(
            LocalDataSource(AppDatabase.getDataBase(this.requireContext()).noteDao()),
            NoteDataSource(ApiClient.service)))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentNoteEditBinding.bind(view)

        binding.btnAddNote.setOnClickListener {
            var note = Note(0,
                binding.editTitle.text.toString(),
                binding.editContent.text.toString(),
                binding.editImageUrl.text.toString())

            viewModel.saveNote(note).observe(viewLifecycleOwner, Observer { result ->

                when(result){
                    is Resource.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        binding.progressbar.visibility = View.GONE
                        findNavController().popBackStack()
                    }
                    is Resource.Failure -> {
                        binding.progressbar.visibility = View.GONE
                        Toast.makeText(requireContext(),"${result.exception.toString()}",Toast.LENGTH_LONG).show()
                    }
                }
            })
        }


    }


}