package me.manenova.notes.repository

import me.manenova.notes.data.local.LocalDataSource
import me.manenova.notes.data.model.Note
import me.manenova.notes.data.model.NoteList
import me.manenova.notes.data.remote.NoteDataSource
import me.manenova.notes.data.model.toNoteEntity

class NoteRepositoryImp(
    private val localDataSource: LocalDataSource,
    private val dataSource: NoteDataSource) : NoteRepository {

    override suspend fun getNotes(): NoteList {
       dataSource.getNotes().data.forEach{ note ->
           localDataSource.saveNote(note.toNoteEntity())
       }

        return localDataSource.getNotes()
    }

    override suspend fun saveNote(note: Note?): Note? {
        return dataSource.saveNote(note)
    }


    // override suspend fun getNotes(): NoteList = dataSource.getNotes()
    // override suspend fun saveNote(note: Note?): Note? = dataSource.saveNote(note)

}