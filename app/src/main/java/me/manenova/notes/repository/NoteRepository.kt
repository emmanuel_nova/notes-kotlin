package me.manenova.notes.repository

import me.manenova.notes.data.model.Note
import me.manenova.notes.data.model.NoteList

interface NoteRepository {

    suspend fun getNotes(): NoteList
    suspend fun saveNote(note: Note?) : Note?

}