package me.manenova.notes.data.remote

import me.manenova.notes.data.model.Note
import me.manenova.notes.data.model.NoteList

class NoteDataSource (private val apiService: ApiService) {

    suspend fun getNotes(): NoteList = apiService.getNotes()

    suspend fun saveNote(note:Note?) : Note? = apiService.saveNote(note)
}