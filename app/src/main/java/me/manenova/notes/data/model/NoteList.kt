package me.manenova.notes.data.model

data class NoteList(val data:List<Note> = listOf())
