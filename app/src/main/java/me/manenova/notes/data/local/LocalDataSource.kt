package me.manenova.notes.data.local

import me.manenova.notes.data.model.NoteEntity
import me.manenova.notes.data.model.NoteList
import me.manenova.notes.data.model.toNoteList

class LocalDataSource (private val noteDao: NoteDao) {

    suspend fun getNotes(): NoteList = noteDao.getNotes().toNoteList()

    suspend fun saveNote(note: NoteEntity)  = noteDao.saveNote(note)
}