package me.manenova.notes.data.remote

import me.manenova.notes.data.model.Note
import me.manenova.notes.data.model.NoteList
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {

    //https://testapi.io/api/manenova/resource/notes
    @GET("resource/notes")
    suspend fun getNotes(): NoteList

    @POST("resource/notes")
    suspend fun saveNote(@Body note: Note?) : Note?
}