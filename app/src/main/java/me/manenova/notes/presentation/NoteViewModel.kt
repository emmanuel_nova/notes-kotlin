package me.manenova.notes.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import me.manenova.notes.core.Resource
import me.manenova.notes.data.model.Note

import me.manenova.notes.repository.NoteRepository

class NoteViewModel (private val repository: NoteRepository) : ViewModel() {

    fun fetchNotes () = liveData(Dispatchers.IO){
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.getNotes()))
        }catch (exception:Exception){
            emit(Resource.Failure(exception))
        }
    }

    fun saveNote(note: Note?) = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.saveNote(note)))
        } catch (exception:Exception){
            emit(Resource.Failure(exception))
        }

    }

}

class NoteViewModelFactory(private val repository: NoteRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(NoteRepository::class.java).newInstance(repository)
    }
}